//Jei wen Wu 1842614

package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		double mag = Math.sqrt(Math.pow(this.getX() ,2) + Math.pow(this.getY(),2) + Math.pow(this.getZ(),2));
		return mag;
	}
	
	public double dotProduct(Vector3d vector) {
		double dotProd = this.getX()*vector.getX() + this.getY()*vector.getY() + this.getZ()*vector.getZ();
		return dotProd;
	}
	
	public Vector3d add(Vector3d vector) {
		double xAdd = this.getX()+vector.getX();
		double yAdd = this.getY()+vector.getY();
		double zAdd = this.getZ()+vector.getZ();
		Vector3d sumVectors = new Vector3d(xAdd, yAdd, zAdd);
		return sumVectors;
	}
	
	public static void main (String[] args) {
		Vector3d testVector= new Vector3d(1,1,2);
		Vector3d testVector2= new Vector3d(2,3,4);
		
		double testMag = testVector.magnitude();
		double testDotProduct = testVector.dotProduct(testVector2);
		Vector3d testAdd = testVector.add(testVector2);
		
		System.out.println(testMag);
		System.out.println(testDotProduct);
		System.out.println(testAdd.toString());
	}
	
	public String toString() {
		String s = "("+getX()+","+getY()+","+getZ()+")";
		return s;
	}

}
