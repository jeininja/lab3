//Jei wen Wu 1842614

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGetXYZ() {
		Vector3d testVector = new Vector3d(2,4,5);
		assertEquals(2, testVector.getX());
		assertEquals(4, testVector.getY());
		assertEquals(5, testVector.getZ());
	}
	
	@Test
	void testMagnitude() {
		Vector3d testVector = new Vector3d(2, 4, 5);
		assertEquals(Math.sqrt(45),testVector.magnitude());
	}
	
	@Test
	void testDotProduct() {
		Vector3d testVector = new Vector3d(1,1,2);
		Vector3d testVector2 = new Vector3d(2,3,4);
		assertEquals(13, testVector.dotProduct(testVector2));
	}
	
	@Test
	void testAdd() {
		Vector3d testVector = new Vector3d(1,1,2);
		Vector3d testVector2 = new Vector3d(2,3,4);
		Vector3d confirmVector = new Vector3d(3,4,6);
		Vector3d resultVector = testVector.add(testVector2);
		assertEquals(confirmVector.toString(), resultVector.toString());
	}

}
